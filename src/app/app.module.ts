import { DatePipe, registerLocaleData } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmpleadosPuestosComponent } from './components/empleados-puestos/empleados-puestos.component';
import { PersonasComponent } from './components/personas/personas.component';
import { PuestosComponent } from './components/puestos/puestos.component';
import { EmpleadoPuestoService } from './components/services/empleado-puesto.service';
import { PersonaService } from './components/services/persona.service';
import { PuestoService } from './components/services/puesto.service';
import localeEs from '@angular/common/locales/es';
import { AngularMaterialModule } from './angular-material/angular-material.module';
registerLocaleData(localeEs, 'es');
@NgModule({
  declarations: [
    AppComponent,
    PersonasComponent,
    PuestosComponent,
    EmpleadosPuestosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [
    PersonaService,
    PuestoService,
    EmpleadoPuestoService,
    { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
    { provide: LOCALE_ID, useValue: 'es-MX' },
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
