import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { EmpleadoPuesto } from '../interfaces/empleado-puesto';
import { Persona } from '../interfaces/persona';
import { Puesto } from '../interfaces/puesto';
import { EmpleadoPuestoService } from '../services/empleado-puesto.service';
import { PersonaService } from '../services/persona.service';
import { PuestoService } from '../services/puesto.service';

@Component({
  selector: 'app-empleados-puestos',
  templateUrl: './empleados-puestos.component.html',
  styleUrls: ['./empleados-puestos.component.scss']
})
export class EmpleadosPuestosComponent implements OnInit {

  form!: FormGroup;
  empleadoPuesto!: EmpleadoPuesto | undefined;
  columnas = ['id', 'puesto', 'persona', 'accion'];
  listEmpleadosPuestos = new MatTableDataSource<EmpleadoPuesto>();
  listPuestos: Puesto[] = [];
  listPersonas: Persona[] = [];
  viewForm: boolean = false;
  view: boolean = false;
  constructor(private empleadoPuestoService: EmpleadoPuestoService, private puestoService: PuestoService,
    private personaService: PersonaService, private fb: FormBuilder) {
  }
  ngOnInit(): void {
    this.listEmpleadosPuestos.data = this.empleadoPuestoService.getAllEmpleadoPuestos();
    this.listPersonas = this.personaService.getAllPersonas();
    this.listPuestos = this.puestoService.getAllPuestos();

  }
  crearForm() {
    this.form = this.fb.group({
      id: [this.getId(), Validators.required],
      puesto: ['', Validators.required],
      persona: ['', Validators.required],
    });
  }
  getId() {
    let id = new Date().getTime().toString();
    return +id.slice(id.length - 4)
  }
  setValores() {
    if (this.empleadoPuesto) {
      let { id, puesto, persona } = this.empleadoPuesto;
      this.form.patchValue({ id, puesto, persona })
    }
  }
  addEmpleadoPuesto() {
    this.viewForm = true;
    this.crearForm();
    this.empleadoPuesto = undefined;
  }
  editEmpleadoPuesto(id: number) {
    this.viewForm = true;
    this.empleadoPuesto = this.empleadoPuestoService.getEmpleadoPuestobyId(id);
    console.log(this.empleadoPuesto)
    this.crearForm();
    this.setValores();
    this.view = false;
  }
  submit() {
    if (this.form.valid) {
      if (this.empleadoPuesto) {
        this.updateEmpleadoPuesto(this.form.value, this.empleadoPuesto.id)
      } else {
        this.insertEmpleadoPuesto(this.form.value)
      }
    }
  }
  insertEmpleadoPuesto(empleadoPuesto: EmpleadoPuesto) {
    this.empleadoPuestoService.insertEmpleadoPuesto(empleadoPuesto)
    this.listEmpleadosPuestos.data = this.empleadoPuestoService.listEmpleadosPuestos;
    this.viewForm = false;
  }
  updateEmpleadoPuesto(empleadoPuesto: EmpleadoPuesto, id: number) {
    this.empleadoPuestoService.updateEmpleadoPuesto(empleadoPuesto, id);
    this.listEmpleadosPuestos.data = this.empleadoPuestoService.listEmpleadosPuestos;
    this.empleadoPuesto = undefined;
    this.viewForm = false;
  }
  getEmpleadoPuestobyId(id: number) {
    this.view = true;
    this.viewForm = true;
    this.empleadoPuesto = this.empleadoPuestoService.getEmpleadoPuestobyId(id);
    this.crearForm()
    this.setValores();
  }
  deleteEmpleadoPuesto(id: number) {
    if (confirm('¿Estas seguro de eliminar el empleado puesto?')) {
      this.empleadoPuestoService.deleteEmpleadoPuesto(id);
      this.listEmpleadosPuestos.data = this.empleadoPuestoService.listEmpleadosPuestos;
    }
  }
  closeView() {
    this.view = false;
    this.viewForm = false;
    this.empleadoPuesto = undefined;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.listEmpleadosPuestos.filter = filterValue.trim().toLowerCase();
  }

}


