import { Injectable } from '@angular/core';
import { Puesto } from '../interfaces/puesto';

@Injectable({
  providedIn: 'root'
})
export class PuestoService {
  listPuestos: Puesto[];
  constructor() {
    this.listPuestos = [];
  }
  getAllPuestos() {
    let value = localStorage.getItem('puestos')
    this.listPuestos = value ? JSON.parse(value) : [];
    return this.listPuestos;
  }
  getPuestobyId(id: number) {
    return this.listPuestos.find((x) => x.id == id);
  }
  insertPuesto(body: Puesto) {
    this.listPuestos.push(body);
    let values = localStorage.getItem('puestos');
    let puestosL = values ? JSON.parse(values) : [];
    puestosL.push(body);
    localStorage.setItem('puestos', JSON.stringify(puestosL));
  }
  updatePuesto(body: Puesto, id: number) {
    let puesto = this.listPuestos.find((element) => (element.id === id));
    if (puesto) {
      puesto.nombre = body.nombre;
      localStorage.setItem('puestos', JSON.stringify(this.listPuestos));
    }
  }
  deletePuesto(id: number) {
    this.listPuestos.forEach((element, index) => {
      if (element.id === id) this.listPuestos.splice(index, 1);
    });
    localStorage.setItem('puestos', JSON.stringify(this.listPuestos));
  }
}

