import { Persona } from "./persona";
import { Puesto } from "./puesto";

export interface EmpleadoPuesto {
  id: number;
  puesto: Puesto;
  persona: Persona;
}
