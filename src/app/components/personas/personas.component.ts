import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Persona } from '../interfaces/persona';
import { PersonaService } from '../services/persona.service';

@Component({
  selector: 'app-personas',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.scss']
})
export class PersonasComponent implements OnInit {
  form!: FormGroup;
  persona!: Persona | undefined;
  columnas = ['id', 'nombre', 'apellidos', 'fecha', 'accion'];
  listPersonas = new MatTableDataSource<Persona>();
  viewForm: boolean = false;
  view: boolean = false;
  constructor(private personasService: PersonaService, private fb: FormBuilder) { }
  ngOnInit(): void {
    this.listPersonas.data = this.personasService.getAllPersonas();
    this.crearForm();
    this.setValores()
  }
  crearForm() {
    this.form = this.fb.group({
      id: [this.getId(), Validators.required],
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      fechaNacimiento: ['', Validators.required]
    });
  }
  getId() {
    let id = new Date().getTime().toString();
    return +id.slice(id.length - 4)
  }
  setValores() {
    if (this.persona) {
      let { id, nombre, apellido, fechaNacimiento } = this.persona;
      this.form.patchValue({ id, nombre, apellido, fechaNacimiento })
    }
  }
  addPerson() {
    this.viewForm = true;
    this.crearForm();
    this.persona = undefined;
  }
  editPerson(id: number) {
    this.viewForm = true;
    this.persona = this.personasService.getPersonabyId(id);
    this.crearForm();
    this.setValores();
    this.view = false;
  }
  submit() {
    if (this.form.valid) {
      if (this.persona) {
        this.updatePersona(this.form.value, this.persona.id)
      } else {
        this.insertPersona(this.form.value)
      }
    }
  }
  insertPersona(persona: Persona) {
    this.personasService.insertPersona(persona)
    this.listPersonas.data = this.personasService.listPersonas;
    this.viewForm = false;
  }
  updatePersona(persona: Persona, id: number) {
    this.personasService.updatePersona(persona, id);
    this.listPersonas.data = this.personasService.listPersonas;
    this.persona = undefined;
    this.viewForm = false;
  }
  getPersonabyId(id: number) {
    this.view = true;
    this.viewForm = true;
    this.persona = this.personasService.getPersonabyId(id);
    this.crearForm()
    this.setValores();
  }
  deletePersona(id: number) {
    if (confirm('¿Estas seguro de eliminar a la persona?')) {
      this.personasService.deletePersona(id);
      this.listPersonas.data = this.personasService.listPersonas;
    }
  }
  closeView() {
    this.view = false;
    this.viewForm = false;
    this.persona = undefined;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.listPersonas.filter = filterValue.trim().toLowerCase();
  }
}
