import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Puesto } from '../interfaces/puesto';
import { PuestoService } from '../services/puesto.service';

@Component({
  selector: 'app-puestos',
  templateUrl: './puestos.component.html',
  styleUrls: ['./puestos.component.scss']
})
export class PuestosComponent implements OnInit {
  form!: FormGroup;
  puesto!: Puesto | undefined;
  columnas = ['id', 'nombre', 'accion'];
  listPuestos = new MatTableDataSource<Puesto>();
  viewForm: boolean = false;
  view: boolean = false;
  constructor(private puestosService: PuestoService, private fb: FormBuilder) {
    this.crearForm();
    this.setValores()
  }
  ngOnInit(): void {
    this.listPuestos.data = this.puestosService.getAllPuestos();
  }
  crearForm() {
    this.form = this.fb.group({
      id: [this.getId(), Validators.required],
      nombre: ['', Validators.required],
    });
  }
  getId() {
    let id = new Date().getTime().toString();
    return +id.slice(id.length - 4)
  }
  setValores() {
    if (this.puesto) {
      let { id, nombre } = this.puesto;
      this.form.patchValue({ id, nombre })
    }
  }
  addPuesto() {
    this.viewForm = true;
    this.crearForm();
    this.puesto = undefined;
  }
  editPuesto(id: number) {
    this.viewForm = true;
    this.puesto = this.puestosService.getPuestobyId(id);
    this.crearForm();
    this.setValores();
    this.view = false;
  }
  submit() {
    if (this.form.valid) {
      if (this.puesto) {
        this.updatePuesto(this.form.value, this.puesto.id)
      } else {
        this.insertPuesto(this.form.value)
      }
    }
  }
  insertPuesto(puesto: Puesto) {
    this.puestosService.insertPuesto(puesto)
    this.listPuestos.data = this.puestosService.listPuestos;
    this.viewForm = false;
  }
  updatePuesto(puesto: Puesto, id: number) {
    this.puestosService.updatePuesto(puesto, id);
    this.listPuestos.data = this.puestosService.listPuestos;
    this.puesto = undefined;
    this.viewForm = false;
  }
  getPuestobyId(id: number) {
    this.view = true;
    this.viewForm = true;
    this.puesto = this.puestosService.getPuestobyId(id);
    this.crearForm()
    this.setValores();
  }
  deletePuesto(id: number) {
    if (confirm('¿Estas seguro de eliminar el puesto?')) {
      this.puestosService.deletePuesto(id);
      this.listPuestos.data = this.puestosService.listPuestos;
    }
  }
  closeView() {
    this.view = false;
    this.viewForm = false;
    this.puesto = undefined;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.listPuestos.filter = filterValue.trim().toLowerCase();
  }
}

