import { Injectable } from '@angular/core';
import { Persona } from '../interfaces/persona';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {
  listPersonas: Persona[] = [];

  constructor() { }

  getAllPersonas() {
    let value = localStorage.getItem('personas')
    this.listPersonas = value ? JSON.parse(value) : [];
    return this.listPersonas;
  }
  getPersonabyId(id: number) {
    return this.listPersonas.find((x) => x.id == id);
  }
  insertPersona(body: Persona) {
    this.listPersonas.push(body);
    let values = localStorage.getItem('personas');
    let personasL = values ? JSON.parse(values) : [];
    personasL.push(body);
    localStorage.setItem('personas', JSON.stringify(personasL));
  }
  updatePersona(body: Persona, id: number) {
    let persona = this.listPersonas.find((element) => (element.id === id));
    if (persona) {
      persona.nombre = body.nombre;
      persona.apellido = body.apellido;
      persona.fechaNacimiento = body.fechaNacimiento;
      localStorage.setItem('personas', JSON.stringify(this.listPersonas));
    }
  }
  deletePersona(id: number) {
    this.listPersonas.forEach((element, index) => {
      if (element.id === id) this.listPersonas.splice(index, 1);
    });
    localStorage.setItem('personas', JSON.stringify(this.listPersonas));
  }
}
