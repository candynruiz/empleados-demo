import { Injectable } from '@angular/core';
import { EmpleadoPuesto } from '../interfaces/empleado-puesto';

@Injectable({
  providedIn: 'root'
})
export class EmpleadoPuestoService {
  listEmpleadosPuestos: EmpleadoPuesto[];
  constructor() {
    this.listEmpleadosPuestos = [];
  }
  getAllEmpleadoPuestos() {
    let value = localStorage.getItem('empleadoPuesto')
    this.listEmpleadosPuestos = value ? JSON.parse(value) : [];
    return this.listEmpleadosPuestos;
  }
  getEmpleadoPuestobyId(id: number) {
    return this.listEmpleadosPuestos.find((x) => x.id == id);
  }
  insertEmpleadoPuesto(body: EmpleadoPuesto) {
    this.listEmpleadosPuestos.push(body);
    let values = localStorage.getItem('empleadoPuesto');
    let empleadoPuestosL = values ? JSON.parse(values) : [];
    empleadoPuestosL.push(body);
    localStorage.setItem('empleadoPuesto', JSON.stringify(empleadoPuestosL));
  }
  updateEmpleadoPuesto(body: EmpleadoPuesto, id: number) {
    let empleadoPuesto = this.listEmpleadosPuestos.find((element) => (element.id === id));
    if (empleadoPuesto) {
      empleadoPuesto.puesto = body.puesto;
      empleadoPuesto.persona = body.persona
      localStorage.setItem('empleadoPuesto', JSON.stringify(this.listEmpleadosPuestos));
    }
  }
  deleteEmpleadoPuesto(id: number) {
    this.listEmpleadosPuestos.forEach((element, index) => {
      if (element.id === id) this.listEmpleadosPuestos.splice(index, 1);
    });
    localStorage.setItem('empleadoPuesto', JSON.stringify(this.listEmpleadosPuestos));
  }
}
